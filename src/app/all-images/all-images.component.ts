import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AllImagesService } from '../services/all-images.service';
import { ImagesService } from '../services/images.service';

@Component({
  selector: 'app-all-images',
  templateUrl: './all-images.component.html',
  styleUrls: ['../search/search.component.css']
})
export class AllImagesComponent implements OnInit {

  public resultats:any;

  constructor(public allImageService:AllImagesService, public  imagesService:ImagesService) { }

  ngOnInit(): void {
      this.allImageService.getAllImages().subscribe( datas=> {
        this.resultats = datas;
      })
  }

}

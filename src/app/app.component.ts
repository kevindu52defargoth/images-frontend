import { Component, OnInit} from '@angular/core';
import {AllKeyWordsService} from './services/all-key-words.service';
import {NgForm} from '@angular/forms'
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  public title = 'images-frontend';
  public resultats:any;
  public search:any;

  constructor(private allKeyWordsService:AllKeyWordsService, private router:Router) {
  }
  ngOnInit(): void {
    this.allKeyWordsService.getAllKeyWords().subscribe(data =>{
      this.resultats = data;
    })
  }

  onSubmit (f:NgForm){
    this.search = f.value.search;
    this.router.navigate(["/search/" + this.search]);
  }

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewComponent } from './view/view.component';
import {HttpClientModule} from "@angular/common/http";
import { SearchComponent } from './search/search.component';
import { AllImagesComponent } from './all-images/all-images.component';
import { FormsModule } from '@angular/forms';
import { UploadComponent } from './upload/upload.component';
import { UploadedComponent } from './uploaded/uploaded.component';


@NgModule({
  declarations: [
    AppComponent,
    ViewComponent,
    SearchComponent,
    AllImagesComponent,
    UploadComponent,
    UploadedComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }


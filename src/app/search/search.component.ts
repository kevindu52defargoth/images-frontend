import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {SearchService} from "../services/search.service";
import {ImagesService} from "../services/images.service";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  public mot:any;
  public resultats:any;

  constructor(public activatedRoute: ActivatedRoute, public searchService: SearchService, public  imagesService:ImagesService) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.mot = params['mot'];
      this.searchService.getSearchKeyWord(this.mot).subscribe( datas=> {
        this.resultats = datas;
      })
  })
  }

}

import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {ImagesService} from "../services/images.service";
import { UploadService } from '../services/upload.service';

@Component({
  selector: 'app-image-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})


export class ViewComponent implements OnInit {

  public image:any;
  public id:any;
  public URLImage:any;
  public nomImage:any;
  public keyWords:any;
  public ajout = false;

  constructor(public uploadService:UploadService, public activatedRoute: ActivatedRoute, public imagesService:ImagesService) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.imagesService.getImageFromId(this.id).subscribe(data =>{
        this.image = data;
        this.nomImage = this.image.name;
        this.URLImage = this.imagesService.imgHost + this.image.fileName + "." + this.image.format;
        this.imagesService.getKeyWordsFromImage(this.image).subscribe(dataK => {
          this.keyWords = dataK;
        })
      })
    });
  }

  onSubmit(f:NgForm) : void {
    const formData = new FormData();

    formData.append("motClef", f.value.motClef.trim());

    formData.append("idImage", this.id);

    this.uploadService.http.post("http://localhost:4200/ajoutKeyword", formData).subscribe(data => {
      console.log(data);
      location.reload();
    });
  }
}

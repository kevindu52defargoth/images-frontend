import { Component, OnInit } from '@angular/core';
import { UploadService } from '../services/upload.service';
import {NgForm} from '@angular/forms'
import { Router } from '@angular/router';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})


export class UploadComponent implements OnInit {

  files:string [] =  [];

  constructor(public uploadService:UploadService, private router:Router) {}

  ngOnInit(): void {
    this.uploadService.http.post(this.uploadService.uploadHost, "jdfsoqfdsqlkj")
  }

  onFilesChange(event:any): void{
    for (var i = 0; i < event.target.files.length; i++) {
      this.files.push(event.target.files[i]);
    }
  }

  onSubmit(f:NgForm): void{
    const formData = new FormData();

    if (f.value.motsClefs.length > 0 && this.files.length > 0) {
      f.value.motsClefs.split(",").forEach((mot:any) => {
        if  (mot.length > 0) {
          formData.append("motsClefs[]", mot.trim());
        }
      })

      for (var i = 0; i < this.files.length; i++) {
        formData.append("file[]", this.files[i]);
      }

      this.uploadService.http.post(this.uploadService.uploadHost, formData).subscribe(data => {
        console.log(data)
      });
      this.router.navigate(["/uploaded"]);
    } else {
      alert("error");
    }
  }
}



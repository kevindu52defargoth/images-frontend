import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {stringify} from "query-string";


@Injectable({
  providedIn: 'root'
})
export class AllKeyWordsService {

  constructor(private http:HttpClient) { }

  public host:string = "http://localhost:8080";

  public getAllKeyWords(){
    return this.http.get(this.host + "/keyWords")
  }
}

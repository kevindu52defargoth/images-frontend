import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  public uploadHost = "http://localhost:4200/uploadAPI";

  constructor(public http:HttpClient) { }


}

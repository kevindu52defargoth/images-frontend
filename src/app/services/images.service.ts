import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {stringify} from "query-string";

@Injectable({
  providedIn: 'root'
})
export class ImagesService {

  public host:string = "http://localhost:8080";

  public imgHost:string = "http://localhost:8000/images/";

  public previewHost:string = "http://localhost:8000/preview/"

  constructor(private http:HttpClient) { }

  public getImageFromId(id:any){
    return this.http.get(this.host + "/images/" + id)
  }

  public getKeyWordsFromImage(i:any){
    return this.http.get(i._links.keyWords.href)
  }
}

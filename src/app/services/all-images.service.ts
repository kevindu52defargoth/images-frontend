import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AllImagesService {

  public host:string = "http://localhost:8080"
  constructor(private http:HttpClient) { }

  public getAllImages(){
    return this.http.get(this.host + "/allImages")
  }
}

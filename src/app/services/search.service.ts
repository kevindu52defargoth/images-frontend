import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  public host:string = "http://localhost:8080"
  constructor(private http:HttpClient) { }

  public getSearchKeyWord(mot:string){
    return this.http.get(this.host + "/search/" + mot)
  }
}

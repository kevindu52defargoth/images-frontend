import { TestBed } from '@angular/core/testing';

import { AllKeyWordsService } from '../all-key-words.service';

describe('AllKeyWordsService', () => {
  let service: AllKeyWordsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AllKeyWordsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

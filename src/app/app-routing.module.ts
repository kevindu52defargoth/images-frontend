import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ViewComponent} from "./view/view.component";
import {SearchComponent} from "./search/search.component";
import {AllImagesComponent} from "./all-images/all-images.component";
import {UploadComponent} from "./upload/upload.component";
import { UploadedComponent } from './uploaded/uploaded.component';

const routes: Routes = [
  {
    path : "view/:id",
    component : ViewComponent
  },
  {
    path : "search/:mot",
    component : SearchComponent
  },
  {
    path : "all",
    component: AllImagesComponent
  },
  {
    path : "upload",
    component: UploadComponent
  },
  {
    path : "uploaded",
    component: UploadedComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
